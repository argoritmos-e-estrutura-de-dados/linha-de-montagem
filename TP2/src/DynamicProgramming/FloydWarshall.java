package DynamicProgramming;
import Graph.JGraph;

/** @author messiah */
public class FloydWarshall {
    public final int MAX_VALUE = 1000;
    private int [][] m, neighbors_nodes;
    int n;
    int weight;
        
    public FloydWarshall(JGraph jg){
        this.weight = 0;
        this.n = jg.getN();
        this.m = new int[this.n][this.n];
        neighbors_nodes = new int[this.n][this.n];
        /** Inicializa a matriz de distancia entre os vértices do grafo jg. */
        for(int i = 0; i < this.n; i++)
            for(int j = 0;j < this.n; j++)
                if(i == j)
                    m[i][j] = 0;
                else if(jg.getE()[i][j] != 0)
                    m[i][j] = jg.getE()[i][j];
                else
                    m[i][j] = MAX_VALUE;
    }    
    
    public void search() throws Exception{
        for(int i = 0; i < this.n; i++)
            for(int j = 0;j < this.n; j++)
                if(this.m[i][j] != 0 && this.m[i][j] != MAX_VALUE)
                    this.neighbors_nodes[i][j] = i;
                else
                    this.neighbors_nodes[i][j] = -1;
        
        /*Para cada vértice i, j e k pertencentes ao grafo, é procurado um caminho de i -> k e k -> j
        que minimiza a distância entre i -> j*/
        
        for(int k = 0; k < this.n; k++)
            for(int i = 0; i < this.n; i++)
                for(int j = 0;j < this.n; j++){           
                    if(this.m[i][k] == MAX_VALUE || this.m[k][j] == MAX_VALUE)
                        continue;
                    if(this.m[i][j]> (this.m[i][k] + this.m[k][j])){
                        this.m[i][j] = m[i][k] + this.m[k][j];
                        this.neighbors_nodes[i][j] = this.neighbors_nodes[k][j];
                    }
                }
    }
    
    public void print(int v0, int v1){
        if(v0 == v1)
            System.out.println(this.neighbors_nodes[v0][v1] + "," + v0 + " -- Distancia: " + this.m[v0][v1]);
        //else if(this.p[i][j]== 0);
        else{
            this.print(v0, this.neighbors_nodes[v0][v1]);
            System.out.println(this.neighbors_nodes[v0][v1] + "," + v1 + " -- Distancia: " + this.m[v0][v1]);
        }            
    }
    
    public void printWeight(int v0, int v1){
        System.out.println("Caminho total percorrido: " + this.m[v0][v1]);
    }    
}
