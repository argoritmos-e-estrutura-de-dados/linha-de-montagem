package DynamicProgramming;

import Graph.JGraph;

/** @author messiah */
public class main {
    public static void main(String[] args) throws Exception {
        System.out.println("Grafo A: ");
        JGraph graph_A = new JGraph(22);
        graph_A.insertEdge(0, 1, 3);
        graph_A.insertEdge(0, 2, 2);
        graph_A.insertEdge(1, 3, 5);
        graph_A.insertEdge(2, 4, 6);
        graph_A.insertEdge(3, 4, 3);
        graph_A.insertEdge(3, 5, 7);        
        graph_A.insertEdge(4, 3, 5);
        graph_A.insertEdge(4, 6, 3);
        graph_A.insertEdge(5, 6, 5);
        graph_A.insertEdge(5, 7, 10);
        graph_A.insertEdge(6, 5, 3);
        graph_A.insertEdge(6, 8, 9);
        graph_A.insertEdge(7, 8, 4);
        graph_A.insertEdge(7, 9, 5);
        graph_A.insertEdge(8, 7, 7);
        graph_A.insertEdge(8, 10, 11);
        graph_A.insertEdge(9, 10, 2);
        graph_A.insertEdge(9, 11, 9);
        graph_A.insertEdge(10, 9, 5);
        graph_A.insertEdge(10, 12, 4);
        graph_A.insertEdge(11, 12, 7);
        graph_A.insertEdge(11, 13, 11);        
        graph_A.insertEdge(12, 11, 6);
        graph_A.insertEdge(12, 14, 9);        
        graph_A.insertEdge(13, 14, 5);        
        graph_A.insertEdge(13, 15, 9);
        graph_A.insertEdge(14, 13, 2);        
        graph_A.insertEdge(14, 16, 3);
        graph_A.insertEdge(15, 16, 8);        
        graph_A.insertEdge(15, 17, 5);
        graph_A.insertEdge(16, 15, 5);        
        graph_A.insertEdge(16, 18, 12);
        graph_A.insertEdge(17, 18, 1);        
        graph_A.insertEdge(17, 19, 2);
        graph_A.insertEdge(18, 17, 2);        
        graph_A.insertEdge(18, 20, 4);
        graph_A.insertEdge(19, 21, 6);
        graph_A.insertEdge(20, 21, 5);
        
        System.out.println("Matriz de Adjacencia:");
        graph_A.print();
        
        System.out.println("\nCaminho minímo utilizando programacao dinamica: ");
        FloydWarshall min_path_graph_A = new FloydWarshall(graph_A);        
        min_path_graph_A.search();
        min_path_graph_A.print(0, 21); 
        min_path_graph_A.printWeight(0, 21);
        
        
        System.out.println("Grafo B: ");
        JGraph graph_B = new JGraph(20);        
        graph_B.insertEdge(0, 1, 5);
        graph_B.insertEdge(0, 2, 7);
        graph_B.insertEdge(1, 3, 10);
        graph_B.insertEdge(2, 4, 3);
        graph_B.insertEdge(3, 4, 4);
        graph_B.insertEdge(3, 5, 6);        
        graph_B.insertEdge(4, 3, 6);
        graph_B.insertEdge(4, 6, 5);
        graph_B.insertEdge(5, 6, 2);
        graph_B.insertEdge(5, 7, 3);
        graph_B.insertEdge(6, 5, 1);
        graph_B.insertEdge(6, 8, 3);
        graph_B.insertEdge(7, 8, 7);
        graph_B.insertEdge(7, 9, 8);
        graph_B.insertEdge(8, 7, 7);
        graph_B.insertEdge(8, 10, 7);
        graph_B.insertEdge(9, 10, 2);
        graph_B.insertEdge(9, 11, 5);
        graph_B.insertEdge(10, 9, 3);
        graph_B.insertEdge(10, 12, 6);
        graph_B.insertEdge(11, 12, 5);
        graph_B.insertEdge(11, 13, 3);        
        graph_B.insertEdge(12, 11, 6);
        graph_B.insertEdge(12, 14, 4);        
        graph_B.insertEdge(13, 14, 8);        
        graph_B.insertEdge(13, 15, 7);
        graph_B.insertEdge(14, 13, 4);        
        graph_B.insertEdge(14, 16, 9);
        graph_B.insertEdge(15, 16, 2);        
        graph_B.insertEdge(15, 17, 12);
        graph_B.insertEdge(16, 15, 5);        
        graph_B.insertEdge(16, 18, 10);
        graph_B.insertEdge(17, 19, 8);        
        graph_B.insertEdge(18, 19, 9);        
        
        System.out.println("Matriz de Adjacencia:");
        graph_A.print();
        
        System.out.println("\nCaminho minímo utilizando programacao dinamica: ");
        FloydWarshall min_path_graph_B = new FloydWarshall(graph_B);        
        min_path_graph_B.search();
        min_path_graph_B.print(0, 19); 
        min_path_graph_B.printWeight(0, 19);
    }
    
}
