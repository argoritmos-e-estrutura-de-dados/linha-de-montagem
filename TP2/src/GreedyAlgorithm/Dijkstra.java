package GreedyAlgorithm;

import Graph.JGraph;

public class Dijkstra {
    /** Armazena as distâncias relativas para cada vértice em cada iteração. */
    private int neighbors_nodes[];
    /** Armazena os nós vizinhos de cada nó do grafo da árvore final produzida. */
    private int relative_distance[];
    
    private JGraph jg;
    int n;
    int weight;
        
    public Dijkstra(JGraph jg){
        this.weight = 0;
        this.jg = jg;
        this.n = jg.getN();        
        this.relative_distance = new int[this.n];        
        this.neighbors_nodes = new int[this.n];        
    }    
    
    public void search(int v0) throws Exception{
        /** Armazena os vértices já verificados.. */
        int verified_vertices[] = new int[this.n+1];
        
        for(int i=0; i<this.n; i++){
            verified_vertices[i+1] = i;
            relative_distance[i] = Integer.MAX_VALUE;
            neighbors_nodes[i] = -1;
        }
        relative_distance[v0] = 0;
       /** Contrói o heap de pesos minímos. */
        Heap jgraph_heap = new Heap(relative_distance, verified_vertices);
        jgraph_heap.build();
        JGraph.Edge adj;
        /** Remove o vertice de valor minimo do heap até que este esteja vazio. */
        while(!jgraph_heap.isEmpty()){            
            int heap_min_value = jgraph_heap.removeMin();
            if(!this.jg.listEdgeisEmpty(heap_min_value)){
                /** adj recebe o primeiro valor da lista de arestas da matriz de adjacência. */
                adj = jg.firstListEdge(heap_min_value);
                /** Enquanto existir algum item na matriz de adjacência. */
                while(adj != null){
                    int v1 = adj.getV1();
                    /** Verifica se os pesos do primeiro item da lista da restas somados a 
                    distanciaRelativa do nó do ponto avaliado é menor que a atual distanciaRelativa
                    do nó vizinho; Caso sim, a distanciaRelativa do nó vizinho ao que está
                    sendo avaliado no momento será atualizada pelo valor dessa nova distancia 
                    avaliada até o pontoAvaliado. */
                    if(relative_distance[v1] > (relative_distance[heap_min_value] + adj.getWeight())){
                        neighbors_nodes[v1] = heap_min_value;
                        jgraph_heap.decreaseKey(v1, relative_distance[heap_min_value] + adj.getWeight());
                    }
                    adj = jg.nextEdge(heap_min_value);
                }
            }
        }
    }
    private int antecessor(int v0) { 
        return this.neighbors_nodes[v0]; 
    }
    
    public void print(int v0, int v1) {
        JGraph.Edge edge;
        if (v0 != v1){
          print (v0, this.neighbors_nodes[v1]);
          edge = jg.existEdge(antecessor(v1), v1);
          this.weight+=edge.getWeight();
          System.out.println("\t" + neighbors_nodes[v1] + "," + v1 + " -- Distancia:"  + edge.getWeight());
        }
    }
    
    public void printWeight(int v0, int v1){
        System.out.println("Caminho total percorrido: " + this.weight);
    }
    
}
